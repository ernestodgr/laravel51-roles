<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Conexion extends Model
{
    protected $table = 'conexion';

    protected $connection = 'pgsql';
	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['ip','user_name','status','created_at'];

    
}
