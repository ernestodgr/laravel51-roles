<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Conexion;
use App\Session;
use Carbon\Carbon;

use Validator;
use Illuminate\Http\Request;
use Illuminate\Cache\RateLimiter;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Support\Facades\Auth;
class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'getLogout']);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|confirmed|min:6',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
    }

    public function getLogin()
    {
        if (view()->exists('auth.authenticate')) {
            return view('auth.authenticate');
        }

        return view('auth.login');
    }

    /**
    *REGISTRA LAS CONEXIONES CUANDO UN USUARIO PUDO LOGEAR EXITOSAMENTE
    **/
    public function conectionRegister($request,$user_name,$status=null){

        Conexion::create([ 'ip'=> $request->ip(),
                             'user_name' => $request->{$user_name},
                             'status' => $status
                            ]);
        return;
    }
    /*
    *REGISTRA EL INICIO DE SESSION
    */
    public function sessionStar(){

        $session_db = Session::create([ 
                          'user_id'   => \Auth::user()->id,
                          'star_login'=> Carbon::now()
                        ]);
        session(['session_db' => $session_db]);
        return;
    }

    public function sessionFinish(){
        $session = Session::find(session('session_db')->id);
        $session->end_login = Carbon::now();
        $session->save();
        return;
    }

    protected function sendLockoutResponse(Request $request)
    {
        $seconds = app(RateLimiter::class)->availableIn(
            $this->getThrottleKey($request)
        );
        $this->conectionRegister($request,$this->loginUsername(),$this->getLockoutErrorMessage($seconds));
        return redirect()->back()
            ->withInput($request->only($this->loginUsername(), 'remember'))
            ->withErrors([
                $this->loginUsername() => $this->getLockoutErrorMessage($seconds),
            ]);
    }

    public function postLogin(Request $request)
    {
        $this->validate($request, [
            $this->loginUsername() => 'required', 'password' => 'required',
        ]);



        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        $throttles = $this->isUsingThrottlesLoginsTrait();

        if ($throttles && $this->hasTooManyLoginAttempts($request)) {
            return $this->sendLockoutResponse($request);
        }

        $credentials = $this->getCredentials($request);

        if (Auth::attempt($credentials, $request->has('remember'))) {
            $this->conectionRegister($request,$this->loginUsername(),"succefull");
            $this->sessionStar();
            return $this->handleUserWasAuthenticated($request, $throttles);

        }

        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
        if ($throttles) {
            $this->incrementLoginAttempts($request);
        }
        $this->conectionRegister($request,$this->loginUsername(),$this->getFailedLoginMessage());
        return redirect($this->loginPath())
            ->withInput($request->only($this->loginUsername(), 'remember'))
            ->withErrors([
                $this->loginUsername() => $this->getFailedLoginMessage(),
            ]);
    }

    public function getLogout()
    {
        Auth::logout();
        $this->sessionFinish();
        return redirect(property_exists($this, 'redirectAfterLogout') ? $this->redirectAfterLogout : '/');
    }
}
