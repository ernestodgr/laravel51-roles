<?php

namespace App\Http\Controllers\Admin;

use Validator;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\User;
use App\Module;
use App\Role;
use App\PermissionType;
use App\Session;
use Carbon\Carbon;

class UserController extends Controller
{
    private $slug = "users"; //slug usuado para obtener los permisos asociados a este controlador en la tabla permissions
    private $user;
    
    public function __construct(){
        $this->user = \Auth::user();
       // $miModule = new Module();
       // dd
        //$module = Module::getPermissionsBySlug($this->slug); //where('slug', $this->slug)->get();
        //$module = Module::where('id',1);
        
        //$this->middleware('permission:create|delete');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        #VERIFICAMOS PERMISOS PARA VER ESTE MODULO
        if (!$this->user->can('user.view')){
            notify()->flash('No posee permisos para visualizar este modulo!', 'info', [
                'timer' => 3000,
                'text' => 'Los privilegios no son suficientes',
            ]);

           return back()->withInput();
        }

        #CONSULTAMOS LA DATA Y LA ENVIAMOS A LA VISTA
        $valor = $request->input('search');


        $users = User::where('name','like',"%$valor%")
                        ->orWhere('email','like',"%$valor%")
                        ->paginate(10);
        
        //CAPTURAMOS DATOS PARA DASHBOARD USUARIO
        $usuarios['total']           = User::all()->count(); //USUARIOS ACTIVOS
        $usuarios['bloqueados']      = User::where('active','=',false)->count(); //USUARIOS BLOQUEADOS
        $usuarios['usuarios_nuevos'] = User::where('created_at','>',Carbon::now()->addDays(-3)->toDateTimeString())->count(); //NUEVOS USUARIOS CON MENOS DE 3 DIAS DE SER INGRESADOS
    
        //dd($users->items());
        //dd($users->items()->attributes());

        return view('admin.user.index',compact('users','request','usuarios'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        #VERIFICAMOS PERMISOS PARA VER ESTE MODULO
        if (!$this->user->can('user.view')){
            notify()->flash('No posee permisos para visualizar este modulo!', 'info', [
                'timer' => 3000,
                'text' => 'Los privilegios no son suficientes',
            ]);
           return back()->withInput();
        }
        
        $roles = Role::where('active','=',true)->get();
        return view('admin.user.create',compact('roles'));
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        #VERIFICAMOS PERMISOS PARA GUARDAR UN NUEVO REGISTRO
        if (!($this->user->can('user.create'))) {
            notify()->flash('No posee permisos para guardar en este modulo!', 'error', [
                'timer' => 3000,
                'text' => 'Los privilegios no son suficientes',
            ]);
            return back()->withInput();
        }

        #VALIDAMOS LA DATA DE ENTRADA SEAN CORRECTA
        $validator = Validator::make($request->all(),[
            'name' => 'required|max:255',
            'last_name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|confirmed|min:6',
        ]);
        
        if ($validator->fails()) {
            return redirect('admin/user/create')
                        ->withErrors($validator)
                        ->withInput();
        }

        #PROCEDEMOS CON EL REGISTRO, SI EL MISMO GENERA UN ERROR SE ENVIA UNA NOTIFICACIÓN POR PANTALLA
        try {
            User::create([
                'name'      => $request->name,
                'last_name' => $request->last_name,
                'email'     => $request->email,
                'active'    => true,
                'password'  => bcrypt($request->password),
            ]);
            notify()->flash('Su registro fue realizado!', 'success', [
                'timer'     => 3000,
                'text'      => '',
            ]);
            return view('admin.user.create');
        }catch(\Illuminate\Database\QueryException $e){
            notify()->flash('Su registro no fue realizado!', 'error', [
                'timer' => 3000,
                'text' => 'Ocurrio un error, por favor revise los datos',
            ]);
            return back()->withErrors($validator)->withInput();
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id,Request $request)
    {
        #VERIFICAMOS PERMISOS PARA VER ESTE MODULO Y SUS DATOS
        if (!($this->user->can('user.view'))) {
            notify()->flash('No posee permisos para visualizar este modulo!', 'info', [
                'timer' => 3000,
                'text' => 'Los privilegios no son suficientes',
            ]);
            return back()->withInput();
        }

        #REALIZAMOS LA CONSULTA DEL USUARIO O LA PERSONA
        $usuario = User::findOrFail($id);
        $roles = Role::where('active','=',true)->get();
        $rol_user = User::find($id)->roles()->get();
        return view('admin.user.show',compact('usuario','roles','rol_user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        #VERIFICAMOS PERMISOS PARA VER ESTE MODULO Y SUS DATOS
        if (!($this->user->can('user.view'))) {
            notify()->flash('No posee permisos para visualizar este modulo!', 'info', [
                'timer' => 3000,
                'text' => 'Los privilegios no son suficientes',
            ]);
           return back()->withInput();
        }

        $modules =  Module::all();
        $permission_type = PermissionType::orderBy('order','asc')->get();
        $mod_array_tree = parseTree($modules->toArray(),0);
        #REALIZAMOS LA CONSULTA DEL USUARIO O LA PERSONA
        $usuario = User::findOrFail($id);
        $rol_user = User::find($id)->roles()->get();
        $roles = Role::where('active','=',true)->get();

        return view('admin.user.edit',compact('usuario','roles','rol_user','mod_array_tree','permission_type'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        #VERIFICAMOS EL PERMISO CORRESPONDIENTE PARA ACTUALIZAR
        /*if (!($this->user->can('user.edit'))) { 
            notify()->flash('No posee permisos para modificar este modulo!', 'error', [
                'timer' => 3000,
                'text' => 'Los privilegios no son suficientes',
            ]);
           return back()->withInput();
        }*/

        #VALIDAMOS QUE EL USUARIO NO PUEDA MODIFICAR SUS PROPIO DATOS
        $user = \Auth::user();
        if($user->id==$id){
            notify()->flash('Su registro no fue realizado!', 'error', [
                'timer' => 3000,
                'text' => "El usuario no puede modificar sus propios datos desde este modulo",
            ]);
            return back()->withInput($request->except('password'));
        }

        #VALIDAMOS LOS DATOS DE ENTRADA SEAN CORRECTOS
        $validator = Validator::make($request->all(),[
            'name'      => 'required|max:255',
            'last_name' => 'required|max:255',
            'email'     => 'required|email|max:255',
            
        ]);

        #SI SE ESCRIBIÓ UN PASSWORD SE LE APLICA LA RESPECTIVA VALIDACION
        if($request->password!=''){
            $validator = Validator::make($request->all(),[
                'password' => 'required|confirmed|min:6',
            ]);
        }
        //EVITA QUE EL ROL CON SLUG 'admin' SEA MODIFICADO
        if($request->slug=='admin'){
            notify()->flash('Su registro no fue realizado!', 'error', [
                'timer' => 3000,
                'text' => "No se puede modificar el slug admin",
            ]);
        }

        if ($validator->fails()) {
            return redirect()->back()
                        ->withErrors($validator)
                        ->withInput($request->except('password'));
        }

        #PROCEDEMOS CON LA ACTUALIZACIÓN, SI EL PROCEDIMIENTO GENERA UN ERROR ESTE ENVIA UN MENSAJE CORRESPONDIENTE
        try {
            $usuario = User::find($id);
            $usuario->name      = $request->name;
            $usuario->last_name = $request->last_name;
            $usuario->email     = $request->email;
            $usuario->password  = $request->password;
            $usuario->active    = $request->active;
            $usuario->save();    
        }catch(\Illuminate\Database\QueryException $e){
            notify()->flash('Su registro no fue realizado!', 'error', [
                'timer' => 3000,
                'text' => "Ocurrio un error, por favor revise los datos",
            ]);
            return back()->withErrors($validator)->withInput($request->except('password'));
        }
        $usuario->detachAllRoles(); //elimina el ó los roles asignados a un usuario
        $usuario->attachRole($request->rol); //asigna un nuevo rol al usuario
        notify()->flash('Su modificación fue realizado!', 'success', [
                'timer' => 3000,
                'text' => '',
        ]);
        return back()->withInput();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Bloquea o desbloquea según sea el estado del usuario
     * @param  [integer] $id
     * @return \Illuminate\Http\Response
     */
    public function status($id){
        #VERIFICAMOS EL PERMISO CORRESPONDIENTE PARA ACTUALIZAR
        if (!($this->user->can('user.view'))) { 
            notify()->flash('No posee permisos para modificar este modulo!', 'error', [
                'timer' => 3000,
                'text' => 'Los privilegios no son suficientes',
            ]);
           return back()->withInput();
        }

        #CONSULTAMOS EL USUARIO Y LO ENVIAMOS A LA VISTA
        $usuario = User::findOrFail($id);
        return view('admin.user.status',compact('usuario'));

    }
}
