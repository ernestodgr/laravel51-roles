<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Validator;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\User;
use App\Module;
use App\Role;

class RolesController extends Controller
{
    private $slug = "users"; //slug usuado para obtener los permisos asociados a este controlador en la tabla permissions
    private $user;
    public function __construct(){
        $this->user = \Auth::user();
       // $miModule = new Module();
       // dd
        //$module = Module::getPermissionsBySlug($this->slug); //where('slug', $this->slug)->get();
        //$module = Module::where('id',1);
        
        //$this->middleware('permission:create|delete');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        #VERIFICAMOS PERMISOS PARA VISUALIZAR ESTE MODULO
        /*if (!$this->user->can('roles.view')){
            notify()->flash('No posee permisos para visualizar este modulo!', 'info', [
                'timer' => 3000,
                'text' => 'Los privilegios no son suficientes',
            ]);
           return back()->withInput();
        }*/

        #CONSULTAMOS LA DATA Y LA ENVIAMOS A LA VISTA
        $valor = $request->input('search');
        //dd($valor);
        $roles = Role::where('name','like',"%$valor%")
                        ->orWhere('slug','like',"%$valor%")
                        ->paginate(10);
        return view('admin.roles.index',compact('roles','request'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        #VERIFICAMOS PERMISOS PARA VER ESTE MODULO
        /*if (!$this->user->can('roles.view')){
            notify()->flash('No posee permisos para visualizar este modulo!', 'info', [
                'timer' => 3000,
                'text' => 'Los privilegios no son suficientes',
            ]);
           return back()->withInput();
        }*/
        
        return view('admin.roles.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        #VERIFICAMOS PERMISOS PARA GUARDAR UN NUEVO REGISTRO
        /*if (!($this->user->can('user.create'))) {
            notify()->flash('No posee permisos para guardar en este modulo!', 'error', [
                'timer' => 3000,
                'text' => 'Los privilegios no son suficientes',
            ]);
            return back()->withInput();
        }*/

        #VALIDAMOS LA DATA DE ENTRADA SEAN CORRECTA
        $validator = Validator::make($request->all(),[
            'name'          => 'required|max:35',
            'slug'          => 'required|max:35',
            'description'   => 'required|max:50',
        ]);
        
        if ($validator->fails()) {
            return redirect('admin/roles/create')
                        ->withErrors($validator)
                        ->withInput();
        }

        #PROCEDEMOS CON EL REGISTRO, SI EL MISMO GENERA UN ERROR SE ENVIA UNA NOTIFICACIÓN POR PANTALLA
        try {
            Role::create([
                'name'          => $request->name,
                'slug'          => $request->slug,
                'description'   => $request->description,
            ]);
            notify()->flash('Su registro fue realizado!', 'success', [
                'timer'     => 3000,
                'text'      => '',
            ]);
            return view('admin.roles.create');
        }catch(\Illuminate\Database\QueryException $e){
            notify()->flash('Su registro no fue realizado!', 'error', [
                'timer' => 3000,
                'text' => 'Ocurrio un error, por favor revise los datos',
            ]);
            return back()->withErrors($validator)->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        #VERIFICAMOS PERMISOS PARA VER ESTE MODULO Y SUS DATOS
        /*if (!($this->user->can('roles.view'))) {
            notify()->flash('No posee permisos para visualizar este modulo!', 'info', [
                'timer' => 3000,
                'text' => 'Los privilegios no son suficientes',
            ]);
            return back()->withInput();
        }*/

        #REALIZAMOS LA CONSULTA DEL USUARIO O LA PERSONA
        $rol = Role::findOrFail($id);
        return view('admin.roles.show',compact('rol'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        #VERIFICAMOS PERMISOS PARA VER ESTE MODULO Y SUS DATOS
        /*if (!($this->user->can('roles.view'))) {
            notify()->flash('No posee permisos para visualizar este modulo!', 'info', [
                'timer' => 3000,
                'text' => 'Los privilegios no son suficientes',
            ]);
           return back()->withInput();
        }*/

        #REALIZAMOS LA CONSULTA DEL USUARIO O LA PERSONA
        $rol = Role::findOrFail($id);
        
        return view('admin.roles.edit',compact('rol'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        #VERIFICAMOS EL PERMISO CORRESPONDIENTE PARA ACTUALIZAR
        /*if (!($this->user->can('role.edit'))) { 
            notify()->flash('No posee permisos para modificar este modulo!', 'error', [
                'timer' => 3000,
                'text' => 'Los privilegios no son suficientes',
            ]);
           return back()->withInput();
        }*/

        #VALIDAMOS LOS DATOS DE ENTRADA SEAN CORRECTOS
        $validator = Validator::make($request->all(),[
            'name' => 'required|max:35',
            'slug' => 'required|max:35',
        ]);

        if ($validator->fails()) {
            return redirect()->back()
                        ->withErrors($validator)
                        ->withInput();
        }

        #PROCEDEMOS CON LA ACTUALIZACIÓN, SI EL PROCEDIMIENTO GENERA UN ERROR ESTE ENVIA UN MENSAJE CORRESPONDIENTE
        try {
            $rol = Role::find($id);
            $rol->name          = $request->name;
            $rol->slug          = $request->slug;
            $rol->description   = $request->description;
            $rol->active        = $request->active;
            $rol->save();

            notify()->flash('Su modificación fue realizado!', 'success', [
                'timer' => 3000,
                'text' => '',
            ]);
            return back()->withInput();
        }catch(\Illuminate\Database\QueryException $e){
            notify()->flash('Su registro no fue realizado!', 'error', [
                'timer' => 3000,
                'text' => 'Ocurrio un error, por favor revise los datos',
            ]);
            return back()->withErrors($validator)->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
