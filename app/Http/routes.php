<?php
use App\Http\Requests;
use Illuminate\Http\Request;

use App\Role;
use App\User;
use App\Module;
use App\Permission;
use App\PermissionType;

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

// Password reset link request routes...
Route::get('password/email', 'Auth\PasswordController@getEmail');
Route::post('password/email', 'Auth\PasswordController@postEmail');
// Password reset routes...
Route::get('password/reset/{token}', 'Auth\PasswordController@getReset');
Route::post('password/reset', 'Auth\PasswordController@postReset');



Route::get('auth/login', 'Auth\AuthController@getLogin');

Route::post('auth/login', ['as' =>'auth/login', 'uses' => 'Auth\AuthController@postLogin']);
Route::get('auth/logout', ['as' => 'auth/logout', 'uses' => 'Auth\AuthController@getLogout']);
 
// Registration routes...
//Route::get('auth/register', 'Auth\AuthController@getRegister');
Route::post('auth/register', ['as' => 'auth/register', 'uses' => 'Auth\AuthController@postRegister']);



Route::get('/', function () {
    return view('layouts/master');
});
Route::get('/home', function () {
    return view('layouts/master');
});

Route::group(['prefix' => 'admin','middleware' => 'auth','namespace'=>'Admin'], function () {
	Route::resource('user','UserController');
	Route::resource('roles','RolesController');
	Route::resource('rol/permission','PermissionsController');
	/*Route::get('user/create','UserController@create');	
	Route::post('user/store','UserController@postRegister');	*/
});
Route::get('permissions_role', function(Request $request){
	if($request->ajax()){
		$role = $request->role;
		$rol_permission = Role::find($role)->permission()->get();
		return $rol_permission;
	}
});
Route::get('permissions_role_update', function(Request $request){
	if($request->ajax()){
		$role 		= $request->role;
		$permission = $request->permission;
		$status 	= $request->status;

		$role 		= Role::find($role);
		$perm_obj 	= Permission::where('slug','=',$permission)->first();
		if( $status == 'true' ){
			$role->attachPermission($perm_obj->id); // permission attached to a role
		}else{
			$role->detachPermission($perm_obj->id); // permission attached to a role
		}
	}
});

Route::get('permissions_user', function(Request $request){
	if($request->ajax()){
		$user = $request->user;
		$role = User::find($user)->roles()->first();

		$permission['role'] = Role::find($role->id)->permission()->get();
		$permission['user'] = User::find($user)->permission()->get();

		return $permission;
	}
});

Route::get('permissions_user_update', function(Request $request){
	if($request->ajax()){
		$user 		= $request->user;

		$permission = $request->permission;
		$status 	= $request->status;
		$user 		= User::find($user);
		$perm_obj 	= Permission::where('slug','=',$permission)->first();

		if( $status == 'true' ){
			$user->attachPermission($perm_obj->id); // permission attached to a role
		}else{
			$user->detachPermission($perm_obj->id); // permission attached to a role
		}
	}
});