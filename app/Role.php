<?php

namespace App;

use Bican\Roles\Models\Role as Roles;

class Role extends Roles
{

	 /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'roles';

    protected $connection = 'pgsql';


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name','slug','description','active'];

    /**
     * obtiene los permisos asociados a un Rol
     *
     * @var array
     */
    public function permission(){
        return $this->belongsToMany('App\Permission','permission_role','role_id','permission_id','permissions');
    }



}
