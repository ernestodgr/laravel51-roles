<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PermissionType extends Model
{
    protected $table = 'permissions_types';
    protected $connection = 'pgsql';
    
    protected $fillable = ['name', 'slug'];

}
