<?php
    use App\Module;

	function parseTree($tree, $root = 0) {
        $return = array();
        foreach($tree as $child => $arr) {
            $parent = $arr['modules_id'];
            if($parent == $root) {
                unset($tree[$child]);
                $return[] = array(
                    'id'        => $arr['id'],
                    'name'      => $arr['name'],
                    'slug'      => $arr['slug'],
                    'children'  => parseTree($tree, $arr['id']),
                );
            }
        }
        return empty($return) ? null : $return;
    }

	$opt="";
	function tablePermission($tree,$re="",$permission_type) {
        global $opt;
        if(!is_null($tree) && count($tree) > 0) {
            $re=$re."<span class='glyphicon glyphicon-minus'></span>";
            foreach($tree as $node) {
            	$background = $node['children']!=null?'background-color:#EDEDED':'';
            	$opt.="<tr style=$background>";
                $opt.="<td data-id='" .$node['id']."'>".$re." ".$node['name'];
                $opt.='</td>';
                foreach ($permission_type as $key => $type) {
                    $perm_modul =  Module::getAccessBySlugType($node['slug'],$type['slug']);
                    /*echo "<pre>";
                    var_dump($perm_modul);
                    echo "</pre>";*/
                    if($perm_modul)
                        $opt.= '<td align="center"><input type="checkbox" name="" id="'.$node['slug']."-".$type['slug'].'" value="" /></td>';
                    else{
                        $opt.="<td align='center'><span class='glyphicon glyphicon-remove'></span></td>";
                    }
                }
                $opt.="</tr>";
                tablePermission($node['children'],$re,$permission_type);
            }
        }
        return $opt;
    }