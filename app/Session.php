<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Session extends Model
{
     /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'sessions';

    protected $connection = 'pgsql';

    public $timestamps = false;

    protected $fillable = ['user_id','star_login','end_login'];

    public function scopeLast_login($query,$user_id){
    	$fecha = \DB::table('sessions')
    					->select(\DB::raw('MAX(star_login)'))
    					->where('user_id','=',$user_id)->first()->max;

    	if($fecha != null)
        	return $query->where('star_login','=', $fecha )->first();
        else
        	return 'false';
    }


}
