<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Bican\Roles\Models\Permission as Permissions;

class Permission extends Permissions
{
	protected $table = 'permissions';
	protected $connection = 'pgsql';
	
    protected $fillable = ['name', 'slug', 'description', 'model','module_id'];
}
