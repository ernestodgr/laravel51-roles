<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Module extends Model
{
	protected $table = 'modules';

    protected $connection = 'pgsql';

	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'slug'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];
    

    /**
     * Obtiene los permisos asociados
     */
    public function permissions()
    {
        return $this->hasMany('App\Permission','module_id','id');
    }
    
    
    public static function getPermissionsBySlug($slug_module){
    	return \DB::table('modules')
    			->select([	'modules.name as module_name',
    						'modules.slug as module_slug',
    						'permissions.name as permissions_name',
    						'permissions.slug as permissions_slug'
    					])
    			->where('modules.slug',$slug_module)
    			->join('permissions', 'permissions.module_id', '=', 'modules.id')
    			->get();
    }
    public static function getAccessBySlugType($slug_module,$type_perm_slug){

        return \DB::table('modules')
            ->select([  'modules.name as module_name',
                            'modules.slug as module_slug',
                            'permissions.name as permissions_name',
                            'permissions.slug as permissions_slug'
                        ])
            ->where('modules.slug',$slug_module)
            ->where('permissions_types.slug',$type_perm_slug)
            ->join('permissions', 'permissions.module_id', '=', 'modules.id')
            ->join('permissions_types', 'permissions.type_id', '=', 'permissions_types.id')
                ->first();
    }
}
