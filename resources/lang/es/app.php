<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Nombre de formulario admin->user
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
    */

    'attribute' => [
        'name'              => 'Nombre',
        'last_name'         => 'Apellido',
        'email'             => 'Correo Electrónico',
        'rol'               => 'Rol',
        'active'            => 'Activo',
        'locked'            => 'Bloqueado',
        'last_login'        => 'Último ingreso',
        'options'           => 'Opciones',
        'total_user'        => 'Usuarios totales',
        'blocked_users'     => 'Usuarios bloquedos',
        'new_users'         => 'Nuevos usuarios',
        'correct_error'     => 'Por favor corrige los errores',
        'description'       => 'Descripción',
        'no_data'           => 'Sin información',
        'create'            => 'Crear',
        'cancel'            => 'Cancelar',
        'user'              => 'Usuario',
        'rol'               => 'Rol',
        'roles'             => 'Roles',
        'find'              => 'Buscar',
        'view'              => 'Ver',
        'status'            => 'Estatus',
        'edit'              => 'Editar',
        'new_user'          => 'Nuevo usuario',
        'create_account'    => 'Crear cuenta',
        'em_fog_pass_ms'    => 'Click here to reset your password',
        'already_a_member'  =>'Ya eres un usuario',
        'login_in'          => 'Inicia sesión',
        'password'          => 'Clave',
        'password_confirmation' => 'Confirmación de Clave',
        'select'            => 'Seleccione',
        'permission_user'   => 'Permisos de usuario',
        'nota_permi_user'   => 'Nota: Aquí podrá habilitar permisos adicionales al ofrecido por el rol',
    ],

    'button' => [
        'view'          => 'Ver',
        'edit'          => 'Editar',
        'delete'        => 'Borrar',
        'lock'          => 'Bloqueo',
        'save'          => 'Guardar',
        'find'          => 'Buscar',
        'cancel'        => 'Cancelar',
        'forggot_pass'  => 'Olvidó su clave?',
        'send'          => 'Enviar',
        'close'         => 'Cerrar',
        'send_pass_rest'=> 'Send Password Reset Link',
        'pass_rest'     => 'Reniciar clave',

    ],

    'title' => [
        'star_session'      => 'Inicio de Sesión',
        'rol_permission'    => 'Permiso de Rol',
        'create'            => 'Crear',
        'roles'             => 'Roles',
        'edit'              => 'Editar',
        'find'              => 'Buscar',
        'to_assign'         => 'Asignar',
        'user'              => 'Usuario ',
        'pass_reset'        => 'Reiniciar clave ',
    ],

];
