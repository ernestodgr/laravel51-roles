
<form action="/auth/login" method="post">
	<h1>{{trans('app.title.star_session')}}</h1>
	@include('partials.menssage_error')
	<div>
	{!! csrf_field() !!}
		<input type="email" name='email' class="form-control" placeholder="{{trans('app.attribute.email')}}" required="" />
	</div>
	<div>
		<input type="password" name='password' class="form-control" placeholder="{{trans('app.attribute.password')}}" required="" />
	</div>
	<div>
		
		<button type="submit" class="btn btn-default submit">{{trans('app.button.send')}}</button>
		<a class="reset_pass" href="{{url('password/email')}}">{{trans('app.button.forggot_pass')}}</a>
	</div>
	<div class="clearfix"></div>
	<div class="separator">
		<p class="change_link">{{trans('app.attribute.new_user')}}? <a href="#toregister" class="to_register">{{trans('app.attribute.create_account')}}</a></p>
		<div class="clearfix"></div>
		<br/>
		<div>
			<h1><i class="" style="font-size: 26px;"></i> Inteldes</h1>
			<p>©2016 Todos los derechos reservados.</p>
		</div>
	</div>
</form>
<!-- form -->
