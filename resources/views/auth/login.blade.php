@extends('layouts/master_login')
@section('content')
     <div class="">
    <a class="hiddenanchor" id="toregister"></a>
    <a class="hiddenanchor" id="tologin"></a>

    <div id="wrapper">
      <div id="login" class="animate form">
        <section class="login_content">
          @include('auth.partials.form_login')
        </section>
        <!-- content -->
      </div>
      <div id="register" class="animate form">
        <section class="login_content">
          @include('auth.partials.register')
        </section>
        <!-- content -->
      </div>
    </div>
  </div>
@endsection