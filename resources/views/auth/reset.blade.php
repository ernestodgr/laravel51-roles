@extends('layouts/master_login')
@section('content')
<div class="">
	<a class="hiddenanchor" id="toregister"></a>
	<a class="hiddenanchor" id="tologin"></a>
	<div id="wrapper">
		<div id="login" class="animate form">
			<section class="login_content">
				<form method="POST" action="/password/reset">
					<h1>{{trans('app.title.pass_reset')}}</h1>
					@include('partials.menssage_error')
					{!! csrf_field() !!}
					<input type="hidden" name="token" value="{{ $token }}">
					<div>
						<input type="email" name="email" value="{{ old('email') }}" class="form-control" placeholder="{{trans('app.attribute.email')}}" required="">
					</div>
					<div>
						<input type="password" name="password" class="form-control" placeholder="{{trans('app.attribute.password')}}">
					</div>
					<div>
						<input type="password" name="password_confirmation" class="form-control" placeholder="{{trans('app.attribute.password_confirmation')}}">
					</div>
					<div>
						<button type="submit" class="btn btn-default">
							{{trans('app.button.pass_rest')}}
						</button>
					</div>
				</form>
			</section>
		</div>
	</div>
</div>
@endsection