@extends('layouts/master_login')
@section('content')
<div class="">
	<a class="hiddenanchor" id="toregister"></a>
	<a class="hiddenanchor" id="tologin"></a>
	<div id="wrapper">
		<div id="login" class="animate form">
			<section class="login_content">
				<form method="POST" action="/password/email">
					<h1>{{trans('app.title.pass_reset')}}</h1>
					@include('partials.menssage_error')
					{!! csrf_field() !!}
					<div>
						<input type="email" name="email" value="{{ old('email') }}" class="form-control" placeholder="Email" required="">
					</div>
					<div>
						<button class="btn btn-default submit" type="submit">
							{{trans('app.button.send_pass_rest')}}
						</button>
					</div>
				</form>
        	</section>
        	<!-- content -->
      	</div>
	</div>
</div>
@endsection