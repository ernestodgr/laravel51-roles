@if (Auth::guest())
	@include('auth/login')
@else
	@include('layouts/app')
@endif