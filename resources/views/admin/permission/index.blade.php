@extends('layouts/master_form')


@section('titulo')
	{{trans('app.title.rol_permission')}}
@endsection

@section('subtitulo')
	{{trans('app.title.to_assign')}}

@endsection

@section('formulario')
	<div class="x_content"><br/>
		<div class="form-group">
			<label for="fullname">{{trans('app.attribute.rol')}} * :</label>
         	<div class="">
				<select class="form-control" required="" name="rol" id="rol">
					@foreach($roles as $key=>$rol)
						<option value="{{$key}}" >{{$rol}}</option>
					@endforeach
				</select>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-12 col-md-12 col-lg-12">
				<div class="col-sm-6 col-md-6 col-md-offset-3 col-lg-6 col-lg-offset-3">
					<?php 
						$table = tablePermission($mod_array_tree,'',$permission_type);
					?>

					<table class="table">
						<tr>
							<td>-</td>
							@foreach($permission_type as $type)
								<td align="center">{{$type['name']}}</td>
							@endforeach
						</tr>

						{!! $table !!}
					</table>
				</div>
			</div>
		</div>
	</div>
@endsection

@section('metodos_js')

		var val = $('#rol').val();	//obtiene el value seleccionado
		if(val!=''){

			$.get("/permissions_role?role=" + val,function(data){
		   		//alert(data);
			}).done(function(data) {
				$(':checkbox').prop("checked", false);
				$.each(data, function( index, value ) {
					//alert( index + ": " + value['slug'] );
					id = value['slug'];
					id = id.replace(".", "-");

					//alert(id);

					$('#'+id).prop("checked", true);
				});
			});
		}else{
			$("#rol").empty();
		}


		$('#rol').change(function(){
			var obj = $(this).attr("id"); //obtiene el id del input selects
			var val = $(this).val();	//obtiene el value seleccionado
			if(val!=''){

				$.get("/permissions_role?role=" + val,function(data){
			   		
				}).done(function(data) {

					$(':checkbox').prop("checked", false);
					$.each(data, function( index, value ) {
						//alert( index + ": " + value['slug'] );
						id = value['slug'];
						id = id.replace(".", "-");

						//alert(id);

						$('#'+id).prop("checked", true);
					});
				});
			}else{
				$("#rol").empty();
			}
		});
		



		$(':checkbox').change(function() {
        	//$('#textbox1').val($(this).is(':checked'));
        	var role = $('#rol').val(); //obtiene el rol del input selects
        	var id = $(this).prop('id'); //refleja el ID del checkbox y tambien es el permiso
        	id = id.replace("-", ".");
        	var status = $(this).prop("checked");


        	$.get("/permissions_role_update?permission=" + id + "&status=" + status + "&role=" + role, function(data){
			   		
			}).done(function(data) {

				//mensaje_update_succefull();
				
				new PNotify({
                            title: 'Actualización exitosa',
                            text: 'Su permiso fue actualizado con éxito',
                            type: 'success'
                        });

			});

			function mensaje_update_succefull(){
				
			}
    	});

	
@endsection