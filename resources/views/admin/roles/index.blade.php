@extends('layouts/master_form')

@section('titulo')
	{{trans('app.title.roles')}}
@endsection

@section('subtitulo')
	{{trans('app.title.find')}}
@endsection

@section('formulario')
<!-- BOTON BUSCAR-->
<div class="col-md-12">
	<form action="{!! url('/admin/roles') !!}" method="get" accept-charset="utf-8">
		<div class="row">
		<div class="col-sm-4" style="border:0px solid red">
			<p><a href="{{url('admin/roles/create')}}" class="btn btn-primary">{{trans('app.attribute.create')}} {{trans('app.attribute.rol')}}</a>	</p>
		</div>
		<div class="col-sm-8" style="border:0px solid red">
			<div class="input-group">
				<input type="text" name='search' class="form-control" required="" value="{{$request->search}}">
				<span class="input-group-btn">
					<button type="submit" class="btn btn-primary">{{trans('app.button.find')}}</button>
				</span>
			</div>
		</div>
		</div>

	</form>
</div>

<!-- TABLA DE REGISTROS-->
<div class="col-md-12">
	@include('admin.roles.partials.list',['status'=>''])
</div>
@endsection