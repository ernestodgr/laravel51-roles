@extends('layouts/master_form')


@section('titulo')
	{{trans('app.title.roles')}}
@endsection

@section('subtitulo')
	{{trans('app.title.create')}}
@endsection

@section('formulario')

<div class="x_content"><br/>
	<form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left" action={{ route('admin.roles.store') }} method="post">
		{!! csrf_field() !!}
		@include('partials.menssage_error')

		@include('admin.roles.partials.fields',['status'=>''])
		<div class="ln_solid"></div>
		<div class="form-group">
			<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
				<a href="{{ URL::previous() }}" class="btn btn-primary">{{trans('app.attribute.cancel')}}</a>
				<button type="submit" class="btn btn-primary">{{trans('app.button.save')}}</button>
			</div>
		</div>
	</form>
</div>

@endsection