@extends('layouts/master_form')


@section('titulo')
	{{trans('app.title.roles')}}
@endsection

@section('subtitulo')
	{{trans('app.title.edit')}}
@endsection


@section('formulario')
<div class="x_content"><br/>
	<form data-parsley-validate class="form-horizontal form-label-left" action="{{route('admin.roles.update',$rol->id)}}" method="POST" accept-charset="utf-8">

		@include('partials.menssage_error')
	
		<input type="hidden" name="_method" value="PUT">
		<input type="hidden" name="_token" value="{{ csrf_token() }}">
		@include('admin.roles.partials.fields',['status'=>''])

		<div class="form-group">
			<label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">{{trans('app.attribute.status')}} <span class="required">*</span></label>
			<div class="col-md-6 col-sm-6 col-xs-12">
				<p>	{{trans('app.attribute.active')}}:<input type="radio" class="flat" name="active" id="statusA" value="1" {!! $rol->active?"checked=":null !!} required /> 
					{{trans('app.attribute.locked')}}:<input type="radio" class="flat" name="active" {!! $rol->active!=1?"checked=":null !!} id="statusB" value="0" />
				</p>
			</div>
		</div>
		<div class="ln_solid"></div>
		<div class="form-group">
			<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
				<a href="{{ URL::previous() }}" class="btn btn-primary">{{trans('app.button.cancel')}}</a>
				<button type="submit" class="btn btn-primary">{{trans('app.button.edit')}}</button>
			</div>
		</div>
	</form>
</div>
@endsection