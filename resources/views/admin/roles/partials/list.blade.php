<table  class="table">
	<thead>
		<tr>
			<th>{{trans('app.attribute.name')}}</th><th>Slug</th><th>{{trans('app.attribute.options')}}</th>
		</tr>
	</thead>
	<tbody>
		@if(count($roles)>0)
			@foreach($roles as $role)
				<tr>
					<td>{{$role->name}}</td>
					<td>{{$role->slug}}</td>
					<td>
						<a href="{{route('admin.roles.show',$role->id)}}" class="btn btn-primary btn-xs"><i class="fa fa-folder"></i> {{trans('app.button.view')}} </a>
                      	<a href="{{route('admin.roles.edit',$role->id)}}" class="btn btn-info btn-xs"><i class="fa fa-pencil"></i> {{trans('app.button.edit')}} </a>
                      	<a href="{{route('admin.roles.edit',$role->id)}}" class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i> {{trans('app.button.delete')}} </a>
                      	<a href="{{url('admin/user/$role->id/status')}}" class="btn btn-warning btn-xs"><i class="glyphicons glyphicons-lock"></i> {{trans('app.button.lock')}} </a>
                   	</td>
				</tr>
			@endforeach
		@else
			<tr>
				<td>{{trans('app.attribute.no_data')}}</td>
			</tr>
		@endif
	</tbody>
</table>