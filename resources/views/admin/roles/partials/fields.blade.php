<!-- input nombre-->
<div class="form-group">
	<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">{{trans('app.attribute.name')}} <span class="required">*</span></label>
	<div class="col-md-6 col-sm-6 col-xs-12">
		<input type="text" name="name" value="{{ $rol->name or old('name') }}" class="form-control col-md-7 col-xs-12" {{ $status }}>
	</div>
</div>
<!-- input slug-->
<div class="form-group">
	<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Slug <span class="required">*</span></label>
	<div class="col-md-6 col-sm-6 col-xs-12">
		<input type="text" name="slug" value="{{$rol->slug or old('slug') }}" class="form-control col-md-7 col-xs-12" {{ $status }}>
	</div>
</div>
<!-- input descripcion-->
<div class="form-group">
	<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">{{trans('app.attribute.description')}} <span class="required">*</span></label>
	<div class="col-md-6 col-sm-6 col-xs-12">
		<input type="text" name="description" value="{{$rol->description or old('description') }}" class="form-control col-md-7 col-xs-12" {{ $status }}>
	</div>
</div>