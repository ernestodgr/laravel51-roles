@extends('layouts/master_form')


@section('titulo')
	{{trans('app.attribute.roles')}}
@endsection

@section('subtitulo')
	{{trans('app.attribute.view')}}
@endsection


@section('formulario')
<div class="x_content"><br/>
	<form data-parsley-validate class="form-horizontal form-label-left" action="{{url('admin.roles.edit')}}" method="get" accept-charset="utf-8">
		@include('admin.roles.partials.fields',['status'=>'disabled'])

		<div class="form-group">
			<label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">{{trans('app.attribute.status')}} <span class="required">*</span></label>
			<div class="col-md-6 col-sm-6 col-xs-12">
			<p>{!! $rol->active?"<div class='alert alert-success' role='alert'><strong>Activo</strong></div>":"<div class='alert alert-danger' role='alert'><strong>Bloqueado</strong></div>" !!}</p>
        	</div>
		</div>

		<div class="ln_solid"></div>
		<div class="form-group">
			<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
				<a href="{{ URL::previous() }}" class="btn btn-primary">{{trans('app.attribute.cancel')}}</a>
				<a href="{{route('admin.roles.edit',$rol->id)}}" class="btn btn-primary" role='button' >{{trans('app.attribute.edit')}}</a>
			</div>
		</div>
	</form>
</div>
@endsection