@extends('layouts/master_form')


@section('titulo')
	{{trans('app.title.user')}}
@endsection

@section('subtitulo')
	{{trans('app.title.edit')}}
@endsection


@section('formulario')
<div class="x_content"><br/>
	<form data-parsley-validate class="form-horizontal form-label-left" action="{{route('admin.user.update',$usuario->id)}}" method="POST" accept-charset="utf-8">

		@include('partials.menssage_error')
	
		<input type="hidden" name="_method" value="PUT">
		<input type="hidden" name="_token" value="{{ csrf_token() }}">
		@include('admin.user.partials.fields',['status'=>'','mensaje'=>'Importante: Si no desea modificar la clave para este usuario deje los campos vacios'])
		
		<div class="form-group">
			<label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">{{trans('app.attribute.status')}} <span class="required">*</span></label>
			<div class="col-md-6 col-sm-6 col-xs-12">
				<p>	{{trans('app.attribute.active')}}:<input type="radio" class="flat" name="active" id="statusA" value="1" {!! $usuario->active?"checked=":null !!} required /> 
	    			{{trans('app.attribute.locked')}}:<input type="radio" class="flat" name="active" {!! $usuario->active!=1?"checked=":null !!} id="statusB" value="0" />
	    			@if(\Auth::user()->id!=$usuario->id)
	    				<a class="btn btn-success btn-sm v_modal" href="#" data-toggle="modal" data-target="#modal_permission"><i class="glyphicon glyphicon-lock"></i> Permisos</a>
	    			@endif
	    		</p>
                  
	    	</div>
    	</div>

    	<div class="form-group">
			<label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">{{trans('app.attribute.password')}} </label>
			<div class="col-md-6 col-sm-6 col-xs-12">
				<input type="password" name="password" class="form-control col-md-7 col-xs-12" {{ $status or "" }} >
			</div>
		</div>

		<div class="form-group">
			<label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">{{trans('app.attribute.password_confirmation')}}</label>
			<div class="col-md-6 col-sm-6 col-xs-12">
				<input type="password" name="password_confirmation" class="form-control col-md-7 col-xs-12" {{ $status or "" }} >
			</div>
		</div>

		<div class="ln_solid"></div>
		<div class="form-group">
			<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
				<a href="{{ URL::previous() }}" class="btn btn-primary">{{trans('app.attribute.cancel')}}</a>
				<button type="submit" class="btn btn-primary">{{trans('app.button.send')}}</button>
			</div>
		</div>
	</form>
</div>

<!-- Modal -->
@permission('permission.view')
<div id="modal_permission" class="modal fade" role="dialog">
 	<div class="modal-dialog">

	    <!-- Modal content-->
	    <div class="modal-content">
			<div class="modal-header">

				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title"><span class="glyphicon glyphicon-lock"></span>{{trans('app.attribute.permission_user')}}</h4>
				<div class='alert alert-warning' role='alert' style='color:black'><strong>{{trans('app.attribute.nota_permi_user')}}</strong></div>
			</div>
			<div class="modal-body">
				<?php 
					$table = tablePermission($mod_array_tree,'',$permission_type);
				?>	
				<table class="table">
					<tr>
						<td>-</td>
						@foreach($permission_type as $type)
							<td align="center">{{$type['name']}}</td>
						@endforeach
					</tr>
					{!! $table !!}
				</table>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">{{trans('app.button.close')}}</button>
			</div>
		</div>
	</div>
</div>
@endpermission

@endsection

@section('metodos_js')
		
		$("#modal_permission").on('show.bs.modal', function () {
            var val = {{$usuario->id}}
			
			if(val!=''){

				$.get("/permissions_user?user=" + val,function(data){
			   		
				}).done(function(data) {
					console.log(data);
					$(':checkbox').prop("checked", false);
					
					$.each(data['role'], function( index, value ) {
						//alert( index + ": " + value['slug'] );
						id = value['slug'];
						id = id.replace(".", "-");

						$( '#'+id ).replaceWith( "<span class='glyphicon glyphicon-ok'></span>" );
						//$( '#'+id ).remove();
						//$('#'+id).prop("checked", true);

					});

					$.each(data['user'], function( index, value ) {
						alert( index + ": " + value['slug'] );

						id = value['slug'];
						id = id.replace(".", "-");

						//$( '#'+id ).remove();
						$('#'+id).prop("checked", true);

					});
				});
			}else{
				//$("#rol").empty();
			}
    	});

		$('.v_modal').click(function(){

			
		});

		$(':checkbox').change(function() {
        	//$('#textbox1').val($(this).is(':checked'));
        	var user = {{$usuario->id}}
        	var id = $(this).prop('id'); //refleja el ID del checkbox y tambien es el permiso
        	id = id.replace("-", ".");
        	var status = $(this).prop("checked");


        	$.get("/permissions_user_update?permission=" + id + "&status=" + status + "&user=" + user, function(data){
			   		
			}).done(function(data) {
				alert(data);
				new PNotify({
                            title: 'Actualización exitosa',
                            text: 'Su permiso fue actualizado con éxito',
                            type: 'success'
                        });
			});
    	});

@endsection