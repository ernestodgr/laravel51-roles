@extends('layouts/master_form')

@section('titulo')
	{{trans('app.attribute.user')}}
@endsection

@section('subtitulo')
	Lista de usuarios
@endsection

@section('formulario')
<!-- BOTON BUSCAR-->
<?php
/*$jsondata = '[{"id":1},{"id":2,"children":[{"id":5,"children":[{"id":7,"children":[{"id":8}]}]},{"id":4},{"id":6},{"id":9},{"id":10}]},{"id":3},{"id":11},{"id":12}]';
$obj = json_decode($jsondata);
dd($obj);*/

?>
	<div>
	<!-- TOTAL DE USUARIO-->
	<div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
   		<div class="tile-stats">
      		<div class="icon"><i class="fa fa-user"></i></div>
          	<div class="count">{{$usuarios['total']}}</div>
          	<h3>{{trans('app.attribute.total_user')}}</h3>
		</div>
	</div>
	<!-- TOTAL DE USUARIO BLOQUEADO-->
	<div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
   		<div class="tile-stats">
      		<div class="icon"><i class="glyphicon glyphicon-lock"></i></div>
          	<div class="count">{{$usuarios['bloqueados']}}</div>
          	<h3>{{trans('app.attribute.blocked_users')}}</h3>
		</div>
	</div>

	<!-- TOTAL DE USUARIO BLOQUEADO-->
	<div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
   		<div class="tile-stats">
      		<div class="icon"><i class="glyphicon glyphicon-star"></i></div>
          	<div class="count">{{$usuarios['usuarios_nuevos']}}</div>
          	<h3>{{trans('app.attribute.new_users')}}</h3>
		</div>
	</div>

	</div>
<div class="col-md-12">

	<form action="{!! url('/admin/user') !!}" method="get" accept-charset="utf-8">
		<div class="row">
		<div class="col-sm-4" style="border:0px solid red">
			<p><a href="{{ route('admin.user.create') }}" class="btn btn-primary">{{trans('app.attribute.create')}} {{trans('app.attribute.user')}}</a>	</p>
		</div>
		<div class="col-sm-8" style="border:0px solid red">
			<div class="input-group">
				<input type="text" name='search' class="form-control" required="" value="{{$request->search}}">
				<span class="input-group-btn">
					<button type="submit" class="btn btn-primary">{{trans('app.button.find')}}</button>
				</span>
			</div>
		</div>
		</div>

	</form>
</div>

<!-- TABLA DE REGISTROS-->
<div class="col-md-12">
	@include('admin.user.partials.list',['status'=>''])
</div>
@endsection