{!! isset($mensaje)?"<div class='alert alert-warning' role='alert' style='color:black'><strong>$mensaje</strong></div>":null !!}

<div class="form-group">
	<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">{{trans('app.attribute.name')}} <span class="required">*</span></label>
	<div class="col-md-6 col-sm-6 col-xs-12">
		<input type="text" name="name" value="{{ $usuario->name or old('name') }}" class="form-control col-md-7 col-xs-12" {{ $status }}>
	</div>
</div>
<div class="form-group">
	<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">{{trans('app.attribute.last_name')}} <span class="required">*</span></label>
	<div class="col-md-6 col-sm-6 col-xs-12">
		<input type="text" name="last_name" value="{{$usuario->last_name or old('last_name') }}" class="form-control col-md-7 col-xs-12" {{ $status }}>
	</div>
</div>
<div class="form-group">
	<label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">{{trans('app.attribute.email')}} <span class="required">*</span></label>
	<div class="col-md-6 col-sm-6 col-xs-12">
		<input type="email" name="email" value="{{$usuario->email or old('email') }}" class="form-control col-md-7 col-xs-12" {{ $status }}>
	</div>
</div>
<div class="form-group">
	<label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">{{trans('app.attribute.rol')}}<span class="required">*</span></label>
	<div class="col-md-6 col-sm-6 col-xs-12">
		<select class="form-control" required="" {{ $status or "" }} name="rol">
			<option value="">{{trans('app.attribute.select')}}</option>
			@foreach($roles as  $rol)
				<?php $estado=""?>
				@if(isset($rol_user))
					@if($rol_user[0]->id==$rol->id)
						<?php $estado="selected"?>
					@endif	
				@endif
				<option value="{{$rol->id}}" {{ $estado }} >{{$rol->name}}</option>
			@endforeach
		</select>
	</div>
</div>