<?php 
	use App\Session; 
?>
<table  class="table">
	<thead>
		<tr>
			<th>{{trans('app.attribute.name')}}</th><th>{{trans('app.attribute.last_name')}}</th><th>{{trans('app.attribute.email')}}</th><th>{{trans('app.attribute.active')}}</th><th>{{trans('app.attribute.last_login')}}</th><th>{{trans('app.attribute.options')}}</th>
		</tr>
	</thead>
	<tbody>
		@if(count($users)>0)
			@foreach($users as $user)
				<tr>
					<td>{{$user->name}}</td>
					<td>{{$user->last_name}}</td>
					<td>{{$user->email}}</td>
					<td align="left">{!!$user->active==true?
						'<span class="glyphicon glyphicon-ok" style="color:#00FF00;"></span>':
						'<span class="glyphicon glyphicon-remove" style="color:#FF0000;"></span>'
					!!}</td>
					<td>{{ Session::last_login( $user->id )!='false'?Session::last_login( $user->id )->star_login:'sin actividad' }}</td>
					<td>
						<a href="{{route('admin.user.show',$user->id)}}" class="btn btn-primary btn-xs"><i class="fa fa-folder"></i> {{trans('app.button.view')}} </a>
                      	<a href="{{route('admin.user.edit',$user->id)}}" class="btn btn-info btn-xs"><i class="fa fa-pencil"></i> {{trans('app.button.edit')}} </a>
                      	<a href="#" class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i> {{trans('app.button.delete')}} </a>
                      	<a href="{{url('admin/user/status',[$user->id])}}" class="btn btn-warning btn-xs"><i class="glyphicons glyphicons-lock"></i> {{trans('app.button.lock')}} 
                   	</td>
				</tr>
			@endforeach
		@else
			<tr>
				<td>not data</td>
			</tr>
		@endif
	</tbody>
</table>