@extends('layouts/master_form')


@section('titulo')
	{{trans('app.attribute.user')}}
@endsection

@section('subtitulo')
	Bloquar
@endsection


@section('formulario')
<div class="x_content"><br/>
	<form data-parsley-validate class="form-horizontal form-label-left" action="{{url('admin.user.edit')}}" method="get" accept-charset="utf-8">
		@include('admin.user.partials.fields',['status'=>'disabled'])

		<div class="ln_solid"></div>
		<div class="form-group">
			<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
				<button type="submit" class="btn btn-primary">{{trans('app.attribute.cancel')}}</button>
				<a href="{{route('admin.user.edit',$usuario->id)}}" class="btn btn-primary" role='button' >{{trans('app.button.edit')}}</a>
			</div>
		</div>
	</form>
</div>
@endsection