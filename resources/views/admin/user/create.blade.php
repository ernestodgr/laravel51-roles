@extends('layouts/master_form')


@section('titulo')
	{{trans('app.attribute.user')}}
@endsection

@section('subtitulo')
	{{trans('app.attribute.create')}}
@endsection

@section('formulario')

<div class="x_content"><br/>
	<form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left" action={{ route('admin.user.store') }} method="post">
		{!! csrf_field() !!}

		@include('partials.menssage_error')
		
		@include('admin.user.partials.fields', array(	'status'=>''))

		<div class="form-group">
			<label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">{{trans('app.attribute.password')}} <span class="required">*</span></label>
			<div class="col-md-6 col-sm-6 col-xs-12">
				<input type="password" name="password" class="form-control col-md-7 col-xs-12" {{ $status or "" }} >
			</div>
		</div>

		<div class="form-group">
			<label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">{{trans('app.attribute.password_confirmation')}}<span class="required">*</span></label>
			<div class="col-md-6 col-sm-6 col-xs-12">
				<input type="password" name="password_confirmation" class="form-control col-md-7 col-xs-12" {{ $status or "" }} >
			</div>
		</div>


		<div class="ln_solid"></div>
		<div class="form-group">
			<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
				<a href="{{ URL::previous() }}" class="btn btn-primary">{{trans('app.attribute.cancel')}}</a>
				<button type="submit" class="btn btn-primary">{{trans('app.button.save')}}</button>
			</div>
		</div>
	</form>
</div>

@endsection