<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConexTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('pgsql')->create('conexion', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->string('ip');

            $table->string('user_name')->nullable(); //usuario que usó o intentó  usar para ingresar

            $table->string('status')->nullable();;

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('pgsql')->drop('conexion');
    }
}
