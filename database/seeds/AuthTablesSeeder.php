<?php

use Illuminate\Database\Seeder;

class AuthTablesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        #USERS
        $userId1 = DB::connection('pgsql')->table('users')->insertGetId([
            'name' 			=> 'Ernesto',
            'last_name'     => 'Gonzalez',
            'email'      	=> 'ernestodgr@hotmail.com',
            'password'      => bcrypt('123456'),
            'created_at'    => '2015-12-01 00:00:00',
            'updated_at'    => '2015-12-01 00:00:00',
        ]);

        $userId2 = DB::connection('pgsql')->table('users')->insertGetId([
            'name'          => 'Pedro',
            'last_name'     => 'Perez',
            'email'         => 'pedro@mail.com',
            'password'      => bcrypt('123456'),
            'created_at'    => '2015-12-01 00:00:00',
            'updated_at'    => '2015-12-01 00:00:00',
        ]);

        #ROLES
        $roleAdminId = DB::connection('pgsql')->table('roles')->insertGetId([
    		'name' 			=> 'Admin',
    		'slug' 			=> 'admin',
    		'description' 	=> '', // optional
    		'level' 		=> 1, // optional, set to 1 by default
    		'created_at'    => '2015-12-01 00:00:00',
            'updated_at'    => '2015-12-01 00:00:00',
        ]);

        $roleUsuarioId = DB::connection('pgsql')->table('roles')->insertGetId([
    		'name' 			=> 'Usuario',
    		'slug' 			=> 'usuario',
    		'description' 	=> '', // optional
    		'level' 		=> 1, // optional, set to 1 by default
    		'created_at'    => '2015-12-01 00:00:00',
            'updated_at'    => '2015-12-01 00:00:00',
        ]);

        #PERMISSION TYPE
        $createTypePermissionId = DB::connection('pgsql')->table('permissions_types')->insertGetId([
			'name' 			=> 'Create',
			'slug' 			=> 'create',
            'order'         => '2',
			'created_at'    => '2015-12-01 00:00:00',
            'updated_at'    => '2015-12-01 00:00:00',
		]);
        $editTypePermissionId = DB::connection('pgsql')->table('permissions_types')->insertGetId([
            'name'          => 'Edit',
            'slug'          => 'edit',
            'order'         => '3',
            'created_at'    => '2015-12-01 00:00:00',
            'updated_at'    => '2015-12-01 00:00:00',
        ]);
        $deleteTypePermissionId = DB::connection('pgsql')->table('permissions_types')->insertGetId([
            'name'          => 'Delete',
            'slug'          => 'delete',
            'order'         => '4',
            'created_at'    => '2015-12-01 00:00:00',
            'updated_at'    => '2015-12-01 00:00:00',
        ]);
        $viewTypePermissionId = DB::connection('pgsql')->table('permissions_types')->insertGetId([
            'name'          => 'View',
            'slug'          => 'view',
            'order'         => '1',
            'created_at'    => '2015-12-01 00:00:00',
            'updated_at'    => '2015-12-01 00:00:00',
        ]);
        
        #MODULES
        $moduleAdminId = DB::connection('pgsql')->table('modules')->insertGetId([
			'name' 			=> 'Admin',
            'slug'          => 'admin',
			'created_at'    => '2015-12-01 00:00:00',
            'updated_at'    => '2015-12-01 00:00:00',
		]);

        $moduleUserId = DB::connection('pgsql')->table('modules')->insertGetId([
            'name'          => 'User',
            'slug'          => 'user',
            'modules_id'    => $moduleAdminId,
            'created_at'    => '2015-12-01 00:00:00',
            'updated_at'    => '2015-12-01 00:00:00',
        ]);

        $moduleRoleId = DB::connection('pgsql')->table('modules')->insertGetId([
            'name'          => 'Role',
            'slug'          => 'role',
            'modules_id'    => $moduleAdminId,
            'created_at'    => '2015-12-01 00:00:00',
            'updated_at'    => '2015-12-01 00:00:00',
        ]);

        $modulePermissionId = DB::connection('pgsql')->table('modules')->insertGetId([
            'name'          => 'Permission',
            'slug'          => 'permission',
            'modules_id'    => $moduleAdminId,
            'created_at'    => '2015-12-01 00:00:00',
            'updated_at'    => '2015-12-01 00:00:00',
        ]);



        #PERMISSIONS

        $viewAdminPermissionId = DB::connection('pgsql')->table('permissions')->insertGetId([
            'name'          => 'Admin view',
            'slug'          => 'admin.view',
            'description'   => '', // optional
            'module_id'     => $moduleAdminId,
            'type_id'       => $viewTypePermissionId, 

            'created_at'    => '2015-12-01 00:00:00',
            'updated_at'    => '2015-12-01 00:00:00',
        ]);

        //------------------------------------------------------------------------
        $viewUserPermissionId = DB::connection('pgsql')->table('permissions')->insertGetId([
            'name'          => 'View users',
            'slug'          => 'user.view',
            'description'   => '', // optional
            'module_id'     => $moduleUserId,
            'type_id'       => $viewTypePermissionId, 

            'created_at'    => '2015-12-01 00:00:00',
            'updated_at'    => '2015-12-01 00:00:00',
        ]);

        $createUserPermissionId = DB::connection('pgsql')->table('permissions')->insertGetId([
			'name'          => 'Create users',
			'slug'          => 'user.create',
			'description'   => '', // optional
            'module_id'     => $moduleUserId,
            'type_id'       => $createTypePermissionId, 

			'created_at'    => '2015-12-01 00:00:00',
            'updated_at'    => '2015-12-01 00:00:00',
		]);

        $editUserPermissionId = DB::connection('pgsql')->table('permissions')->insertGetId([
            'name'          => 'Edit users',
            'slug'          => 'user.edit',
            'description'   => '', // optional
            'module_id'     => $moduleUserId,
            'type_id'       => $editTypePermissionId, 

            'created_at'    => '2015-12-01 00:00:00',
            'updated_at'    => '2015-12-01 00:00:00',
        ]);

		$deleteUserPermissionId = DB::connection('pgsql')->table('permissions')->insertGetId([
			'name'          => 'Delete users',
			'slug'          => 'user.delete',
            'description'   => '', // optional
            'module_id'     => $moduleUserId,
            'type_id'       => $deleteTypePermissionId, 

			'created_at'    => '2015-12-01 00:00:00',
            'updated_at'    => '2015-12-01 00:00:00',
		]);
        
        //------------------------------------------------------------------------

        $viewRolePermissionId = DB::connection('pgsql')->table('permissions')->insertGetId([
            'name'          => 'Role view',
            'slug'          => 'role.view',
            'description'   => '', // optional
            'module_id'     => $moduleRoleId,
            'type_id'       => $viewTypePermissionId, 

            'created_at'    => '2015-12-01 00:00:00',
            'updated_at'    => '2015-12-01 00:00:00',
        ]);

        $createRolePermissionId = DB::connection('pgsql')->table('permissions')->insertGetId([
            'name'          => 'Role create',
            'slug'          => 'role.create',
            'description'   => '', // optional
            'module_id'     => $moduleRoleId,
            'type_id'       => $createTypePermissionId, 

            'created_at'    => '2015-12-01 00:00:00',
            'updated_at'    => '2015-12-01 00:00:00',
        ]);

        $editRolePermissionId = DB::connection('pgsql')->table('permissions')->insertGetId([
            'name'          => 'Role edit',
            'slug'          => 'role.edit',
            'description'   => '', // optional
            'module_id'     => $moduleRoleId,
            'type_id'       => $editTypePermissionId, 

            'created_at'    => '2015-12-01 00:00:00',
            'updated_at'    => '2015-12-01 00:00:00',
        ]);

        $deleteRolePermissionId = DB::connection('pgsql')->table('permissions')->insertGetId([
            'name'          => 'Role delete',
            'slug'          => 'role.delete',
            'description'   => '', // optional
            'module_id'     => $moduleRoleId,
            'type_id'       => $deleteTypePermissionId, 

            'created_at'    => '2015-12-01 00:00:00',
            'updated_at'    => '2015-12-01 00:00:00',
        ]);

        //------------------------------------------------------------------------

        $viewPermissionPermissionId = DB::connection('pgsql')->table('permissions')->insertGetId([
            'name'          => 'Permission view',
            'slug'          => 'permission.view',
            'description'   => '', // optional
            'module_id'     => $modulePermissionId,
            'type_id'       => $viewTypePermissionId, 

            'created_at'    => '2015-12-01 00:00:00',
            'updated_at'    => '2015-12-01 00:00:00',
        ]);

        $createPermissionPermissionId = DB::connection('pgsql')->table('permissions')->insertGetId([
            'name'          => 'Permission create',
            'slug'          => 'permission.create',
            'description'   => '', // optional
            'module_id'     => $modulePermissionId,
            'type_id'       => $createTypePermissionId, 

            'created_at'    => '2015-12-01 00:00:00',
            'updated_at'    => '2015-12-01 00:00:00',
        ]);

        $editPermissionPermissionId = DB::connection('pgsql')->table('permissions')->insertGetId([
            'name'          => 'Permission edit',
            'slug'          => 'permission.edit',
            'description'   => '', // optional
            'module_id'     => $modulePermissionId,
            'type_id'       => $editTypePermissionId, 

            'created_at'    => '2015-12-01 00:00:00',
            'updated_at'    => '2015-12-01 00:00:00',
        ]);

        $deletePermissionPermissionId = DB::connection('pgsql')->table('permissions')->insertGetId([
            'name'          => 'Permission delete',
            'slug'          => 'permission.delete',
            'description'   => '', // optional
            'module_id'     => $modulePermissionId,
            'type_id'       => $deleteTypePermissionId, 

            'created_at'    => '2015-12-01 00:00:00',
            'updated_at'    => '2015-12-01 00:00:00',
        ]);
        
        #PERMISSION ROLE

        DB::connection('pgsql')->table('permission_role')->insertGetId([
            'permission_id' => $viewAdminPermissionId,
            'role_id'       => $roleAdminId,
            'created_at'    => '2015-12-01 00:00:00',
            'updated_at'    => '2015-12-01 00:00:00',
        ]);

    		DB::connection('pgsql')->table('permission_role')->insertGetId([
    			'permission_id' => $createUserPermissionId,
    			'role_id' 		=> $roleAdminId,
    			'created_at'    => '2015-12-01 00:00:00',
                'updated_at'    => '2015-12-01 00:00:00',
    		]);

            DB::connection('pgsql')->table('permission_role')->insertGetId([
                'permission_id' => $viewUserPermissionId,
                'role_id'       => $roleAdminId,
                'created_at'    => '2015-12-01 00:00:00',
                'updated_at'    => '2015-12-01 00:00:00',
            ]);

            DB::connection('pgsql')->table('permission_role')->insertGetId([
                'permission_id' => $editUserPermissionId,
                'role_id'       => $roleAdminId,
                'created_at'    => '2015-12-01 00:00:00',
                'updated_at'    => '2015-12-01 00:00:00',
            ]);

            DB::connection('pgsql')->table('permission_role')->insertGetId([
                'permission_id' => $deleteUserPermissionId,
                'role_id'       => $roleAdminId,
                'created_at'    => '2015-12-01 00:00:00',
                'updated_at'    => '2015-12-01 00:00:00',
            ]);

            DB::connection('pgsql')->table('permission_role')->insertGetId([
                'permission_id' => $viewRolePermissionId,
                'role_id'       => $roleAdminId,
                'created_at'    => '2015-12-01 00:00:00',
                'updated_at'    => '2015-12-01 00:00:00',
            ]);

            DB::connection('pgsql')->table('permission_role')->insertGetId([
                'permission_id' => $viewPermissionPermissionId,
                'role_id'       => $roleAdminId,
                'created_at'    => '2015-12-01 00:00:00',
                'updated_at'    => '2015-12-01 00:00:00',
            ]);

        

        #ROL VS USUARIO
        DB::connection('pgsql')->table('role_user')->insertGetId([
            'role_id'       => $roleAdminId,
            'user_id'       => $userId1,
        ]);
        DB::connection('pgsql')->table('role_user')->insertGetId([
            'role_id'       => $roleAdminId,
            'user_id'       => $userId2,
        ]);

    }
}
